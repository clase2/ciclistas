<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Ciclista;
use app\models\Puerto;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    public function actionPersonal()
    {
        return $this->render('personal');
    }
    
    public function actionPersonas(){
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => Ciclista::find()->select("edad")->distinct(),
            'pagination' =>[
                   'pageSize'=>5,
            ]            
        ]);
        
        return $this -> render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 1 con Active Record",
            "enunciado"=>"Listar las edades de los ciclistas (sin repetidos)",
            "sql"=>"SELECT DISTINCT edad FROM ciclista",
            
        ]);
    }
    
     public function actionConsulta2a(){
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => Ciclista::find()->select("edad")->where("nomequipo='Artiach'"),
            'pagination' =>[
                   'pageSize'=>5,
            ]            
        ]);
        
        return $this -> render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 2 con Active Record",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach",
            "sql"=>"SELECT edad FROM ciclista WHERE nomequipo = 'Artiach'",
            
        ]);
    }
    public function actionConsulta3a(){
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => Ciclista::find()->select("edad")->where("nomequipo='Artiach' OR nomequipo = 'Amore Vita'"),
            'pagination' =>[
                   'pageSize'=>5,
            ]            
        ]);
        
        return $this -> render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 3 con Active Record",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach o de Amore Vita.",
            "sql"=>"SELECT edad FROM ciclista WHERE nomequipo = 'Artiach' OR nomequipo = 'Amore Vita'",
            
        ]);
    }
    public function actionConsulta4a(){
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => Ciclista::find()->select("edad")->where("edad < 25 OR edad > 30"),
            'pagination' =>[
                   'pageSize'=>5,
            ]            
        ]);
        
        return $this -> render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 4 con Active Record",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30.",
            "sql"=>"SELECT dorsal FROM ciclista WHERE edad < 25 OR edad > 30",
            
        ]);
    }
    public function actionConsulta5a(){
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => Ciclista::find()->select("edad")->where("(edad >28 AND edad < 32) AND nomequipo = 'Banesto'"),
            'pagination' =>[
                   'pageSize'=>5,
            ]            
        ]);
        
        return $this -> render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 5 con Active Record",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto.",
            "sql"=>"SELECT dorsal FROM ciclista WHERE (edad >28 AND edad < 32) AND nomequipo = 'Banesto'",
            
        ]);
    }
    public function actionConsulta6a(){
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => Ciclista::find()->select("nombre")->where("length(SUBSTRING_INDEX(nombre,' ', 1)) > 8"),
            'pagination' =>[
                   'pageSize'=>5,
            ]            
        ]);
        
        return $this -> render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['nombre'],
            "titulo"=>"Consulta 6 con Active Record",
            "enunciado"=>"Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8.",
            "sql"=>"SELECT nombre FROM ciclista WHERE length(SUBSTRING_INDEX(nombre,' ', 1)) > 8",
            
        ]);
    }
    public function actionConsulta7a(){
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => Ciclista::find()->select("nombre,dorsal"),
            'pagination' =>[
                   'pageSize'=>5,
            ]            
        ]);
        
        return $this -> render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['nombre','dorsal'],
            "titulo"=>"Consulta 7 con Active Record",
            "enunciado"=>"Lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas.",
            "sql"=>"SELECT UPPER(nombre),dorsal FROM ciclista",
            
        ]);
    }
    public function actionConsulta8a(){
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => Ciclista::find()->select("edad")->where("edad < 25 OR edad > 30"),
            'pagination' =>[
                   'pageSize'=>5,
            ]            
        ]);
        
        return $this -> render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 8 con Active Record",
            "enunciado"=>"Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa.",
            "sql"=>"SELECT dorsal FROM ciclistas WHERE edad < 25 OR edad > 30",
            
        ]);
    }
    public function actionConsulta9a(){
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => Puerto::find()->select("nompuerto")->where("altura > 1500"),
            'pagination' =>[
                   'pageSize'=>5,
            ]            
        ]);
        
        return $this -> render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['nompuerto'],
            "titulo"=>"Consulta 9 con Active Record",
            "enunciado"=>"Listar el nombre de los puertos cuya altura sea mayor de 1500.",
            "sql"=>"SELECT nompuerto FROM puerto WHERE altura > 1500;",
            
        ]);
    }
    public function actionConsulta10a(){
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => Ciclista::find()->select("edad")->where("edad < 25 OR edad > 30"),
            'pagination' =>[
                   'pageSize'=>5,
            ]            
        ]);
        
        return $this -> render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 10 con Active Record",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000.",
            "sql"=>"SELECT dorsal FROM puerto WHERE pendiente  > 8 OR altura > 1800 OR altura < 3000 ;",
            
        ]);
    }
    public function actionConsulta11a(){
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => Ciclista::find()->select("edad")->where("edad < 25 OR edad > 30"),
            'pagination' =>[
                   'pageSize'=>5,
            ]            
        ]);
        
        return $this -> render("resultado",[
           "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 4 con Active Record",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30.",
            "sql"=>"SELECT dorsal FROM ciclistas WHERE edad < 25 OR edad > 30",
            
        ]);
    }
}
